#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <windows.h>

int main()
{
    int tamY = 23;
    int tamX = 40;
    int x, y, yi;
    char world[tamY][tamX];
    char nome[50];
    char jogador = 'A';
    char jogadorLaser = '^';
    char inimigo = 'M';
    char inimigoescudo = 'O';
    char inimigolaser = 'U';
    char explosao = 'X';
    int score = 0;
    int victory = 1;
    int laserpronto = 1;
    int inimigopronto = 0;

    srand(time(NULL));


     system("color 10");

    printf("\n\t___________________________________________________\n");
    printf("\t_____________________________________________________\n");
    printf("\t___BEM VINDOS AO JOGO COM MELHOR INTERFACE GRAFICA___\n");
    printf("\t_____________________________________________________\n");
    printf("\t_________________                     _______________\n");
    printf("\t_________________   SPACE INVADERS   ________________\n");
    printf("\t_____________________________________________________\n");
    printf("\t_____________________________________________________\n");
    printf("\t\nCriado Por :Cleanne, Edna, Matheus e Vitor \n\n");
    printf("\tEscolha uma das opcoes abaixo:\n\n");
    printf("\t%c 1 Jogar.\n\t%c 2 Ajuda.\n\t%c 3 Sair.\n\n",16,16,16);
    printf("\opcao: ");
    scanf("%d",&jogador);
    system("cls");

     switch(jogador){

                case 1:

                     system("color 50");
                     printf("\n\n\t\tINFORME O NOME DO JOGADOR:\n\n\t\t\t\t",jogador);
                     scanf("%d",jogador);
                     printf("\n\n\n");
                     system("pause");
                     system("cls");
                     system("color 3");
                     printf("\t_________________________________________________________\n");
                     printf("\t_________________________________________________________\n");
                     printf("\t___Espero que esteja pronto(a) para iniciar sua jornada__\n");
                     printf("\n\n\n");
                     printf("\t______________________ BOA VIAGEM _______________________\n");
                     printf("\t_________________________________________________________\n");
                     system("pause");
                     printf("\n\n\n");
                     system("cls");
                     break;
                case 2:
                     system("color 90");
                     printf("\n___________  O OBJETIVO DO JOGO  ___________");
                     printf("\n\n\t      MATHEUS TU COLOCA AQUI O OBJETIVO r!\n\n");

                     printf("\n___________REGRAS O JOGO____________");
                     printf("\n\n\t1 E AQUI COLOCA AS REGRAS VISSE!!!!!!!c\n");
                     printf("__________________________\n\n");
                     system("pause");
                     system("cls");
                     break;

                case 3:
                     system("color 70");
                     Beep(1000,1500);
                      printf("\t__________________________________\n");
                      printf("\t__________________________________\n");
                      printf("\t__________________________________\n");
                      printf("\t_______                    _______\n");
                      printf("\t_______                    _______\n");
                      printf("\t_______     FIM DE JOGO    _______\n");
                      printf("\t_______                    _______\n");
                      printf("\t__________________________________\n");
                      printf("\t__________________________________\n");
                      scanf("%s",jogador);
                     system("pause");
                     system("cls");
                     break;


                     }


    /*iniciando */
    int totalEnemies = 0;
    for (x = 0; x < tamX; x ++) {
        for (y = 0; y < tamY; y ++) {
            if ((y+1) % 2 == 0 && y < 7 && x > 4
            && x < tamX - 5 && x % 2 ==0) {
                world[y][x] = inimigo;
                totalEnemies ++;
            }
            else if ((y+1) % 2 == 0 && y >= 7 && y < 9 && x > 4
            && x < tamX - 5 && x % 2 ==0){
                world[y][x] = inimigoescudo;
                totalEnemies = totalEnemies + 2;
            }
            else {
                world[y][x] = ' ';
            }
        }
    }
    world[tamY - 1][tamX / 2] = jogador;
    int i = 1;
    char direction = 'l';
    char keyPress;
    int inimigosAtuais = totalEnemies;
    while(inimigosAtuais > 0 && victory) {
        int drop = 0;
        int enemySpeed = 1 + 10 * inimigosAtuais / totalEnemies;
        laserpronto ++;


        system("cls");
        printf("     SCORE:    %d", score);
        printf("\n");
            for (y = 0; y < tamY; y ++) {
            printf("|");
                for (x = 0; x < tamX; x ++) {
                    printf("%c",world[y][x]);
                }
            printf("|");
            printf("\n");
            }

        /*laser */
        for (x = 0; x < tamX; x ++) {
            for (y = tamY-1; y >= 0; y --) {
                if (i%2 == 0 && world[y][x] == inimigolaser
                && (world[y+1][x] != inimigo & world[y+1][x] != inimigoescudo)){
                world[y+1][x] = inimigolaser;
                world[y][x] = ' ';
                }
                else if (i%2 == 0 && world[y][x] == inimigolaser
                && (world[y+1][x] == inimigo | world[y+1][x] == inimigoescudo)){
                    world[y][x] = ' ';
                }
            }
        }
        for (x = 0; x < tamX; x ++) {
            for (y = 0; y < tamY; y ++) {
                if ((i % 5) == 0 && (world[y][x] == inimigoescudo
                | world[y][x] == inimigo) && (rand() % 15) > 13
                && world[y+1][x] != jogadorLaser) {
                    for (yi = y+1; yi < tamY; yi ++) {
                        if (world[yi][x] == inimigo
                        | world[yi][x] == inimigoescudo) {
                            inimigopronto = 0;
                            break;
                        }
                        inimigopronto = 1;
                    }
                    if (inimigopronto) {
                        world[y+1][x] = inimigolaser;
                    }
                }
                if (world[y][x] == jogadorLaser && world[y-1][x] == inimigo) {
                    world[y][x] = ' ';
                    world[y-1][x] = explosao;
                    inimigosAtuais --;
                    score = score + 50;
                }
                else if (world[y][x] == jogadorLaser
                && world[y-1][x] == inimigoescudo) {
                    world[y][x] = ' ';
                    world[y-1][x] = inimigo;
                    inimigosAtuais --;
                    score = score + 50;
                }
                else if (world[y][x] == jogadorLaser
                && world[y-1][x] == inimigolaser) {
                    world[y][x] = ' ';
                }
                else if (world[y][x] == explosao) {
                    world[y][x] = ' ';
                }
                else if ((i+1) % 2 == 0 && world[y][x] == inimigolaser
                && world[y+1][x] == jogador) {
                    world[y+1][x] = explosao;
                    world[y][x] = ' ';
                    victory = 0;
                }
                else if (world[y][x] == jogadorLaser
                && world[y-1][x] != inimigolaser) {
                        world[y][x] = ' ';
                        world[y-1][x] = jogadorLaser;
                }
            }
        }


        for (y = 0; y < tamY; y ++) {
            if (world[y][0] == inimigo) {
                direction = 'r';
                drop = 1;
                break;
            }
            if (world[y][tamX-1] == inimigo){
                direction = 'l';
                drop = 1;
                break;
            }
        }


        if (i % enemySpeed == 0) {
            if (direction == 'l') {
                for (x = 0; x < tamX - 1; x ++) {
                    for (y = 0; y < tamY; y ++) {
                        if (drop && (world[y-1][x+1] == inimigo
                            || world[y-1][x+1] == inimigoescudo)){
                            world[y][x] = world[y-1][x+1];
                            world[y-1][x+1] = ' ';
                        }
                        else if (!drop && (world[y][x+1] == inimigo
                            || world[y][x+1] == inimigoescudo)) {
                            world[y][x] = world[y][x+1];
                            world[y][x+1] = ' ';
                        }
                    }
                }
            }
            else {
                for (x = tamX; x > 0; x --) {
                    for (y = 0; y < tamY; y ++) {
                        if (drop && (world[y-1][x-1] == inimigo
                            || world[y-1][x-1] == inimigoescudo)) {
                            world[y][x] = world[y-1][x-1];
                            world[y-1][x-1] = ' ';
                        }
                        else if (!drop && (world[y][x-1] == inimigo
                            || world[y][x-1] == inimigoescudo)) {
                            world[y][x] = world[y][x-1];
                            world[y][x-1] = ' ';
                        }
                    }
                }
            }
            for (x = 0; x < tamX; x ++) {
                if (world[tamY - 1][x] == inimigo) {
                    victory = 0;
                }
            }
        }


        if(kbhit()){
            keyPress = getch();
        }
        else {
            keyPress = ' ';
        }
        if (keyPress == 'a') {
            for (x = 0; x < tamX; x = x+1) {
                if ( world[tamY-1][x+1] == jogador) {
                    world[tamY-1][x] = jogador;
                    world[tamY-1][x+1] = ' ';
                }
            }
        }

        if (keyPress == 'd') {
            for (x = tamX - 1; x > 0; x = x-1) {
                if ( world[tamY-1][x-1] == jogador) {
                    world[tamY-1][x] = jogador;
                    world[tamY-1][x-1] = ' ';
                }
            }
        }
        if (keyPress == 'm' && laserpronto > 2) {
            for (x = 0; x < tamX; x = x+1) {
                if ( world[tamY-1][x] == jogador) {
                    world[tamY - 2][x] = jogadorLaser;
                    laserpronto = 0;
                }
            }
        }
        i ++;
        Sleep(50);
    }
    system("cls");
        printf("     SCORE:    %d", score);
        printf("\n");
            for (y = 0; y < tamY; y ++) {
            printf("|");
                for (x = 0; x < tamX; x ++) {
                    printf("%c",world[y][x]);
                }
            printf("|");
            printf("\n");
            }
    Sleep(1000);
    system("cls");
    if (victory != 0) {
        printf("\n \n \n \n \n \n               CONGRATULATIONS! \n \n \n \n \n");
        Sleep(1000);
        printf("\n \n               Score: %d", score);
        Sleep(1000);
        int bonus = totalEnemies*20 - i;
        printf("\n \n               Bonus: %d", bonus);
        Sleep(1000);
        printf("\n \n               Total Score: %d", score + bonus);
        printf("\n \n \n \n               Well done");
        Sleep(1000);
        printf(", Hero.");
        Sleep(1000);
        getch();
    }
    else {
        printf(nome);
        printf("\n \n \n \n \n \n               You have failed with this Ship.");
        Sleep(1000);
        printf("\n \n \n \n \n \n               Voce morreu .");
        Sleep(1000);
        printf("\n \n               Final Score: %d", score);
        getch();
    }
}
